import {ADD_TODO, REMOVE_TODO, UPDATE_TODO} from './type';
let index = 0;


export function addTodo(text: string){
    return {type: ADD_TODO, payload : {id: index++, content: text}};
}

export function removeTodo(id: number){
    return {type: REMOVE_TODO, payload: {id: index}};
}

export function updateTodo(id: number, text: string){
    return {type: UPDATE_TODO, payload: {id: index, content: text}};
}