import { Button } from "react-bootstrap";
import React from "react";
import { Card } from "react-bootstrap";

type TodoListProps = {
    txt: string,
    onDelete: (index: number) => void;
    updaterTrigger: () => void;
    onUpdate: (index: number, txt: string) => void,
    updateText: string,
    index: number
}


class TodoList extends React.Component<TodoListProps, {}>{
    render(): React.ReactNode{
        return(
            <div>
                <Card>
                {this.props.txt}
                <span>
                    <Button variant="outline-danger"onClick={() => this.props.onDelete(this.props.index)}> DELETE</Button>
                    <Button variant="outline-primary" onClick={() => {this.props.updaterTrigger(); if(this.props.updateText.length>0) this.props.onUpdate(this.props.index,this.props.updateText)}}> UPDATE </Button>
                </span>
                </Card>
            </div>
        )
    }
}

export default TodoList;