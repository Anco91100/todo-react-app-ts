export type addTodo = (text: string) => {type: string, payload : {id: number, content: string}}
export type removeTodo = (id: number) => {type: string, payload: {id: number}}
export type updateTOdo = (id: number, text: string) => {type: string, payload: {id: number, text: string}}