import React, { ChangeEvent } from "react";
import {Form, Button} from 'react-bootstrap'
import { connect } from "react-redux";
import { addTodo, removeTodo, updateTodo } from "../redux/actions/todoAction";
import TodoList from "./TodoList";

type TodoProps = {
    addTodo: typeof addTodo,
    removeTodo: typeof removeTodo,
    updateTodo: typeof updateTodo,
    data: any
}

type TodoState = {
    txt : string
}

class Todo extends React.Component<TodoProps, TodoState> {
    constructor(props: TodoProps){
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }
    
    handleChange(e: ChangeEvent<HTMLInputElement>){
        this.setState({
            txt: e.target.value.trim()
        });
    }

    handleUpdate(){
        this.setState({
            txt: ''
        });
    }

    render(): React.ReactNode {
        return (
            <div>
            <Form onSubmit={(e)=> {
                e.preventDefault();
                this.props.addTodo(this.state.txt)
                this.setState({
                    txt: ''
                });
            }}>
                <Form.Group className="mb-1" controlId="formBasicText">
                <Form.Control
                value={this.state.txt}
                onChange={this.handleChange}
                type="text"
                placeholder="Enter todo"
                />
                <Button variant="outline-success" type="submit">
                    Ajouter 
                </Button>
                </Form.Group>
            </Form>
            {this.props.data &&
            this.props.data.todo.map((item: { content: string; index: number; })=>{
                let content = item.content.trim()
                return content.length ? <TodoList txt={content} updaterTrigger={this.handleUpdate} updateText={content} key={item.index} index={item.index} onUpdate={this.props.updateTodo} onDelete={this.props.removeTodo} /> : false
            })
            }
         </div> 
        )
    }
}


const mapDispatchToProps = (dispatch: (arg0: { type: string; payload: { id: number; content: string; } | { id: number; content: string; } | { id: number; }; }) => any) =>{
    return {
        addTodo: (text: string) => dispatch(addTodo(text)),
        updateTodo: (id: number,text: string) => dispatch(updateTodo(id,text)),
        removeTodo: (id: number) => dispatch(removeTodo(id))
    }
};

const mapStateToProps = (state: any) => ({
    data: state
});


export default connect(mapStateToProps,mapDispatchToProps)(Todo)